# README

This project contains materials I prepared as part of an internship at A1QA and trainings at QA BootCamp.

## Aquaducks project

This project contains materials after manual tesing of web app for booking trips:

*   test survey
*   quality report

## Photostudio project

This project contains materials after manual tesing of web app for booking photoshoots:

*   acceptance sheet
*   test survey
*   quality report

## Voyage project

This project contains materials after manual tesing of web app for managing schools database and publishing travel opportunities:

*   some of the bug reports 
*   quality report

## SQL requests

This directory contains SQL requests I've written for test database.