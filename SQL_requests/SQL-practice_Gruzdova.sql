Задача 1
select employee_id, last_name, first_name, job_id, hire_date as START_DATE 
from employees;

Задача 2
select employee_id || ' ' || first_name || ' ' || last_name || ' ' || email || ' ' || phone_number || ' ' || hire_date || ' ' || job_id || ' ' || salary || ' ' || commission_pct || ' ' || manager_id || ' ' || department_id as output
from employees;

Задача 3а
select employee_id, last_name, salary, salary*1.15 as new_salary
from employees;

Задача 3b
select employee_id, last_name, salary, salary*1.15 as new_salary, salary*1.15-salary as increase
from employees;

Задача 4
select *
from employees
where department_id in (60,230,210) and salary > 5000 or department_id in (10) and salary <= 9000;

Задача 5
select last_name || ' earns ' || salary || ' monthly but wants ' || salary*3 as report
from employees;

Задача со
select employee_id, last_name, job_id,
        case when job_id='AD_PRES' then 'A'
             when job_id='ST_MAN' then 'B'
             when job_id='IT_PROG' then 'C'
             when job_id='SA_REP' then 'D'
             when job_id='ST_CLERK' then 'E'
             else '0'
             end as employee_level
from employees;