Задача 1
select max(salary) as "Maximum", min(salary) as "Minimum", round(avg(salary)) as "Average", sum(salary) as "Sum" 
from employees
group by department_id;

Задача 2
select job_id, max(salary) as "Maximum", min(salary) as "Minimum", round(avg(salary)) as "Average", sum(salary) as "Sum" 
from employees
group by job_id;

Задача 3
select trunc(months_between(max(start_date), min(start_date))) as Difference
from job_history;

Задача 4
select job_id, min(salary)
from employees
group by job_id
having min(salary)>5000
order by 2 desc;

Задача со*
select department_id, 
        min(case when job_id='ST_MAN' then salary end) as "ST_MAN",
        min(case when job_id='MK_MAN' then salary end) as "MK_MAN",
        min(case when job_id='SA_REP' then salary end) as "SA_REP",
        min(case when job_id='ST_CLERK' then salary end) as "ST_CLERK",
        sum(salary) as "Total"
from employees
where department_id is not null
group by department_id
order by 1;