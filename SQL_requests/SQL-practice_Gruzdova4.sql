Задача 0
delete from copy_employees_gruzdova;

Задача 1
insert into copy_employees_gruzdova
select * 
from employees
where salary > (select avg(e.salary)
                from employees e join departments d
                on e.department_id=d.department_id
                where d.department_name like '%Executive%');
rollback;

Задача 2
insert into copy_employees_gruzdova
select *
from employees
where employee_id in (SELECT employee_id
                        FROM job_history
                        GROUP BY employee_id
                        HAVING COUNT(employee_id) > 1);
commit;

Задача 3
update copy_employees_gruzdova
set hire_date = sysdate;
rollback;


Задача 4
 delete from copy_employees_gruzdova 
    where salary in (select min(salary) from copy_employees_gruzdova);


