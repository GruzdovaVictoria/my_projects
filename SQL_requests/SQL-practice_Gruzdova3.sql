Задача 1
select d.department_id, d.department_name, d.location_id, l.street_address, l.postal_code, l.city, country_name
from departments d left join locations l
on l.location_id = d.location_id
join countries c
on c.country_id = l.country_id;

Задача 2
select distinct m.first_name, m.last_name
from employees e join employees m 
on m.employee_id = e.manager_id
where m.salary > 10000;

Задача *
select last_name, hire_date
from employees
where hire_date > (select hire_date
                    from employees
                    where last_name = 'Greenberg')
order by 2 asc; 

Задача 3
select e.last_name, e.department_id, e.job_id
from employees e join departments d
on e.department_id = d.department_id
where location_id = 1400;

Задача 4
select e.department_id, e.last_name, e.job_id, d.department_name
from employees e join departments d
on e.department_id = d.department_id
where d.department_name = 'Administration';

Задача **
Решение без подзапроса:
select e.department_id, d.department_name, count(e.department_id) as Employees_count, d.location_id, l.street_address, l.postal_code, l.city, c.country_name
from employees e join departments d
on e.department_id = d.department_id
join locations l
on l.location_id = d.location_id
join countries c
on c.country_id = l.country_id
group by e.department_id, d.department_name, d.location_id, l.street_address, l.postal_code, l.city, c.country_name;



